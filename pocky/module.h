#pragma once

#include "inc.h"

class Module {
public:
	// c/tor
	__forceinline Module( uintptr_t base, uintptr_t size, const std::string &name, const std::string &path ) :
		m_base( base ),
		m_size( size ),
		m_name( name ),
		m_path( path ) {
	}
	
private:
	uintptr_t m_base = 0;
	uintptr_t m_size = 0;
	std::string m_name;
	std::string m_path;

public:
	// gets 
	__forceinline uintptr_t get_image_base() {
		return m_base;
	}

	__forceinline uintptr_t get_image_size() {
		return m_size;
	}

	__forceinline const std::string &get_image_name() {
		return m_name;
	}

	__forceinline const std::string &get_image_path() {
		return m_path;
	}
};