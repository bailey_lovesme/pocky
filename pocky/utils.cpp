#include "utils.h"

std::string Utils::get_file_path() {
	char buffer[ MAX_PATH ];
	GetModuleFileName( 0, buffer, MAX_PATH );
	std::string::size_type pos = std::string( buffer ).find_last_of( "\\/" );
	return std::string( buffer ).substr( 0, pos );
}

Utils g_utils;