#pragma once

#include "inc.h"
#include "module.h"

#define INRANGE(x,a,b)		(x >= a && x <= b) 
#define bit_from_pattern( x )		(INRANGE( x,'0','9' ) ? ( x - '0' ) : ( ( x & ( ~0x20 ) ) - 'A' + 0xa ))
#define byte_from_pattern( x )		(bit_from_pattern( x [ 0 ] ) << 4 | bit_from_pattern( x [ 1 ] ))

// thank you to maddie for the sig scanning
class Memory {
public:
	__forceinline Memory( HANDLE proc ) : m_proc( proc ) { }

	__forceinline ~Memory() {
		// close process handle
		CloseHandle( m_proc );
	}

private:
	HANDLE m_proc = nullptr;

public: // read process memory
	template<typename t> __forceinline t read( uintptr_t address ) {
		t read_data;
		ReadProcessMemory( m_proc, reinterpret_cast<void *>( address ), &read_data, sizeof( t ), 0 );
		return read_data;
	}

	template<typename t> __forceinline t read( uintptr_t address, int dereferences ) {
		while ( dereferences-- )
			address = read<uintptr_t>( address );

		read<t>( address );
	}

	__forceinline bool read( uintptr_t address, uintptr_t size, void *buffer ) {
		return ReadProcessMemory( m_proc, reinterpret_cast<void *>( address ), &buffer, size, 0 ) != 0;
	}

public: // write process memory
	template<typename t> __forceinline void write( ulong_t address, t value ) {
		WriteProcessMemory( m_proc, reinterpret_cast<void *>( address ), &value, sizeof( t ), 0 );
	}

	__forceinline bool write( uintptr_t address, uintptr_t size, void *buffer ) {
		return WriteProcessMemory( m_proc, reinterpret_cast<void *>( address ), &buffer, size, 0 ) != 0;
	}

public: // allocate - remove read/write restrictions
		// https://msdn.microsoft.com/en-us/library/windows/desktop/aa366890(v=vs.85).aspx
	__forceinline uintptr_t allocate( uintptr_t size ) {
		return reinterpret_cast<uintptr_t>( VirtualAllocEx( m_proc, 0, size, MEM_COMMIT, PAGE_READWRITE ) );
	}

public: // signature scanning
	__forceinline uintptr_t scan( uintptr_t start, const char *pattern, uintptr_t range );
	__forceinline uintptr_t scan( Module *module, const char *pattern );
};

uintptr_t Memory::scan( uintptr_t start, const char *pattern, uintptr_t range ) {
	uint8_t *buffer = new( std::nothrow ) uint8_t[ range ];
	auto pattern_byte = reinterpret_cast<const uint8_t *>( pattern );

	if ( !read( start, range, buffer ) ) {
		delete [ ] buffer;
		return 0;
	}

	uint8_t *match;
	for ( auto i = buffer; i < reinterpret_cast<uint8_t *>( i + range ); i++ ) {
		__try {
			if ( *(uint8_t *)pattern_byte == 63 || *i == byte_from_pattern( pattern_byte ) ) {
				if ( !match )
					match = i;

				pattern_byte += ( *(uint16_t *)pattern_byte == 16191 || *(uint8_t *)pattern_byte != 63 ) ? 0x3 : 0x2;

				if ( !*pattern_byte ) {
					uintptr_t return_address = ( match - buffer ) + start;
					delete [ ] buffer;
					return return_address;
				}
			} else if ( match ) {
				i = match;
				pattern_byte = reinterpret_cast<uint8_t *>( const_cast<char *>( pattern ) );
				match = 0;
			}
		} __except ( 1 ) {};
	}

	delete [ ] buffer;
	return 0;
}

uintptr_t Memory::scan( Module *module, const char *pattern ) {
	return scan( module->get_image_base(), pattern, module->get_image_size() );
}