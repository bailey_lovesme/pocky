#pragma once

// abstract base class for hacks to inherit from
class Hack {
private:
	// whether or not this hack is enabled
	bool		m_enabled;

public:
	// c/tor
	Hack( bool enabled ) : m_enabled( enabled ) {}

	// d/tor
	virtual ~Hack();

	virtual void on_call() {}

public:
	// put this as reference so we can overwrite it in other functions if needed
	__forceinline bool &enabled() {
		return m_enabled;
	}
};