#pragma once

// utils - our app
#include "application.h"

// utils - process related
#include "process.h"
#include "input.h"

// hack main
#include "hack_main.h"