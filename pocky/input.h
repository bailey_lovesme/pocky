#pragma once

#include "inc.h"

class Input {
private:
	std::array<bool, 256> m_keys;
public:
	__forceinline bool key( size_t key ) {
		return m_keys.at( key );
	}

	// called in main loop
	__forceinline void think();
};

void Input::think() {
	for ( int i = 255; i; --i )
		m_keys[ i ] = GetAsyncKeyState( i ) != 0;
}

extern Input g_input;