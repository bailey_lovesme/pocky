#pragma once

#include "inc.h"
#include "hack_base.h"
#include "netvars.h"

class Pocky {
public:
	// c/tor
	__forceinline Pocky();

	// think
	__forceinline void think();

	// d/tor
	__forceinline ~Pocky();

private:
	// all of our cheat's registered hacks
	std::vector<std::shared_ptr<Hack>> m_hacks;

public:
	std::shared_ptr<Netvars> m_netvars;
};

Pocky::Pocky() {
	// lambda to register a hack pointer to our vector
	static auto register_hack = [&]( Hack *hack ) {
		m_hacks.push_back( std::make_shared<Hack>( hack ) );
	};

	// usage e.g -> register_hack( Bhop );

	// load netvars
	m_netvars = std::make_shared<Netvars>();
}

void Pocky::think() {
	for ( auto &it : m_hacks ) {
		// get this itteration's hack pointer
		auto hack = it.get();

		// if it's not enabled then skip it
		if ( !hack->enabled() )
			continue;

		// run hack
		hack->on_call();
	}
}

Pocky::~Pocky() {
	for ( auto &it : m_hacks ) {
		auto hack = it.get();

		// call the destructor for the hack if one exists
		hack->~Hack();
	}
}

extern Pocky g_pocky;