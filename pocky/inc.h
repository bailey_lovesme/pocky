#pragma once

// general includes
#include <iostream>
#include <algorithm>
#include <chrono>
#include <thread>
#include <memory>
#include <Windows.h>
#include <wincon.h>
#include <stdarg.h>
#include <TlHelp32.h>
#include <Psapi.h>

#pragma warning(disable: 4996)

// types
#include <string>
#include <map>
#include <vector>
#include <deque>
#include <unordered_map>
#include <stdint.h>
#include <array>
using ulong_t = unsigned long;

// utils - function helpers
#include "utils.h"

