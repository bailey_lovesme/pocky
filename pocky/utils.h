#pragma once

#include "inc.h"

class Utils {
public:
	// get current program's file path
	static std::string get_file_path();
};

extern Utils g_utils;