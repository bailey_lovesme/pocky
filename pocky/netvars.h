#pragma once

#include "inc.h"

class Netvars {
public:
	// c/tor
	__forceinline Netvars();

private:
	// map containing all of our netvars, assigned to their name
	std::map<const char *, uintptr_t> m_netvars;
public:
	// get a netvar
	__forceinline uintptr_t get( const char *name ) {
		return m_netvars[ name ];
	}
};

Netvars::Netvars() {
	const char *file = std::string( g_utils.get_file_path() + "\\netvars.ini" ).c_str();

	static auto register_netvar = [&]( const char *netvar_name ) {
		uintptr_t value = GetPrivateProfileIntA( "netvars", netvar_name, 0, file );
		m_netvars.emplace( netvar_name, value );

		auto output = std::string( netvar_name ) + " -> 0x%p\n";
		g_app.output( output, value );
	};

	// e.g. -> register_netvar( "m_iHealth" );
}