#include "pocky.h"

// console applicationn -> pocky
Application g_app{ "pocky" };

// process -> csgo.exe
Process		g_proc{ "csgo.exe" };

// user input handling
Input		g_input;

// main hack stuff
Pocky		g_pocky;

int main() {
	while ( true ) {
		std::this_thread::sleep_for( std::chrono::microseconds( 1 ) );

		// no point running the hack if the game isnt our foreground window
		if ( GetForegroundWindow() != g_proc.get_window() )
			continue;

		// run our input think to check for which keys are currently pressed
		g_input.think();

		// update all our hacks
		g_pocky.think();
	}
	return 0;
}