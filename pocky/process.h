#pragma once

#include "inc.h"
#include "module.h"
#include "memory.h"

class Process {
	using modules_t = std::vector<std::shared_ptr<Module>>;
public:
	// c/tor
	Process();
	Process( const std::string &proc_name );

	// d/tor
	~Process();

private: // process vars
	// game's process ID
	ulong_t m_proc_id = 0;

	// our handles to the game
	HANDLE	m_proc = nullptr;
	HWND	m_game = nullptr;

public: // gets
	__forceinline HWND get_window() const {
		return m_game;
	}

	__forceinline HANDLE get_process() const {
		return m_proc;
	}

	__forceinline ulong_t get_id() const {
		return m_proc_id;
	}

public: // process attatch/detatch
	// attatch to game process
	bool attatch( const std::string &proc_name );

	// detatch from game process
	void detatch();

private:
	// get this process' id by name
	ulong_t get_proc_id( const std::string &proc_name );

private: // modules
	// vector to store module info
	modules_t m_modules;

	// get game modules' info
	bool get_modules();

	// get a module from the vector by image name
	Module *get_module( const std::string &module_name );

	Module *m_client = nullptr;
	Module *m_engine = nullptr;

public: // get modules
	__forceinline Module *client() const {
		return m_client; 
	}

	__forceinline Module *engine() const {
		return m_engine;
	}

	__forceinline modules_t get_module_list() const {
		return m_modules;
	}

public: // process memory
	std::shared_ptr<Memory> m_memory;

};

extern Process g_proc;