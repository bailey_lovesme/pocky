#pragma once

#include "inc.h"

// the console window of our application
class Application {
public:
	// enum containing some colours that we can use
	// used in SetConsoleTextAttribute
	enum Colour_t : int {
		BLACK = 0,
		BLUE,
		GREEN,
		CYAN,
		RED,
		MAGENTA,
		BROWN,
		LIGHTGRAY,
		DARKGRAY,
		LIGHTBLUE,
		LIGHTGREEN,
		LIGHTCYAN,
		LIGHTRED,
		LIGHTMAGENTA,
		YELLOW,
		WHITE,
	};

private:
	// handles to our console window
	HANDLE		m_output			= nullptr;
	HANDLE		m_input				= nullptr;

	// original colour attributes of the console
	// todo: store off background if i ever intend on modifying that
	Colour_t	m_original_colour	= WHITE;

public:
	// c/tor
	__forceinline Application( const std::string &name = "" );

private:
	// set the colour of text in the console
	__forceinline void set_text_colour( const Colour_t &colour ) {
		if ( !m_output )
			return;
		
		// set colour to passed colour
		SetConsoleTextAttribute( m_output, colour );
	}
	
public:
	// print text to output
	// todo - seems kinda hacky to have two funcs, but im not sure how I could assign a default parameter considering i have a varying number of additional arguments
	__forceinline void output( const std::string text, ... );
	__forceinline void output( const Colour_t &colour, const std::string text, ... );
};

Application::Application( const std::string &name ) {
	// if we have a name passed, set the window's name to that
	if ( !name.empty() )
		SetConsoleTitleA( name.c_str() );

	m_input = GetStdHandle( STD_INPUT_HANDLE );
	m_output = GetStdHandle( STD_OUTPUT_HANDLE );

	CONSOLE_SCREEN_BUFFER_INFO info;
	GetConsoleScreenBufferInfo( m_output, &info );
	m_original_colour = static_cast<Colour_t>( info.wAttributes );

	if ( !name.empty() )
		output( LIGHTMAGENTA, "%s initialising\n", name.c_str() );
}

void Application::output( const std::string text, ... ) {
	// nothing to print
	if ( text.empty() || !m_output )
		return;

	// set up list to hold information about variable arguments
	va_list list;

	// initialise variable argument list
	va_start( list, text );

	// write formatted data from list to string
	// https://www.programiz.com/cpp-programming/library-function/cstdio/vsnprintf
	int size = std::vsnprintf( 0, 0, text.c_str(), list );

	// create a buffer with our size
	auto buffer = new char [ size + 1 ];
	if ( !buffer ) {
		va_end( list );
		return;
	}

	// print text to buffer
	std::vsnprintf( buffer, size + 1, text.c_str(), list );

	// end list
	va_end( list );

	// print
	WriteConsoleA( m_output, buffer, size + 1, nullptr, nullptr );

	// delete our buffer
	delete [ ] buffer;
}

void Application::output( const Colour_t &colour, const std::string text, ... ) {
	// nothing to print or invalid output handle
	if ( text.empty() || !m_output )
		return;

	// set up list to hold information about variable arguments
	va_list list;

	// initialise variable argument list
	va_start( list, text );

	// write formatted data from list to string
	// https://www.programiz.com/cpp-programming/library-function/cstdio/vsnprintf
	int size = std::vsnprintf( 0, 0, text.c_str(), list );

	// create a buffer with our size
	auto buffer = new char[ size + 1 ];
	if ( !buffer ) {
		va_end( list );
		return;
	}

	// print text to buffer
	std::vsnprintf( buffer, size + 1, text.c_str(), list );

	// end list
	va_end( list );

	// set our text colour
	set_text_colour( colour );

	// print
	WriteConsoleA( m_output, buffer, size + 1, nullptr, nullptr );

	// delete our buffer
	delete [ ] buffer;

	// reset our text colour
	set_text_colour( m_original_colour );
}

extern Application g_app;