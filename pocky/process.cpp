#include "process.h"
#include "application.h"

Process::Process() {
	// attatch to the game process
	if ( !attatch( "csgo.exe" ) )
		exit( 1 );
}

Process::Process( const std::string &proc_name ) {
	if ( !attatch( proc_name ) )
		exit( 1 );
}

Process::~Process() {
	detatch();
}

bool Process::attatch( const std::string &proc_name ) {
	if ( proc_name.empty() || ( m_game && m_proc && m_engine && m_client && !m_modules.empty() ) )
		return false;

	// get our target process' process id
	m_proc_id = get_proc_id( proc_name );
	if ( m_proc_id == 0 )
		return false;

	// get handle to the game window
	m_game = FindWindowA( "Valve001", 0 );
	if ( m_game == INVALID_HANDLE_VALUE )
		return false;

	m_proc = OpenProcess( PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_VM_OPERATION | PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION, 0, m_proc_id );
	if ( m_proc == INVALID_HANDLE_VALUE )
		return false;

	// get all modules
	if ( !get_modules() )
		return false;

	// get client and engine module pointers
	m_client = get_module( "client.dll" );
	m_engine = get_module( "engine.dll" );

	g_app.output( g_app.LIGHTMAGENTA, "modules:\n" );
	g_app.output(	"client.dll -> 0x%p\n"
					"engine.dll -> 0x%p\n",
					m_client, m_engine
	);

	if ( !m_client || !m_engine )
		return false;

	// make a unique pointer to our process memory class
	m_memory = std::make_shared<Memory>( m_proc );
	if ( !m_memory )
		return false;

	g_app.output( g_app.LIGHTMAGENTA, "created handle to %s\n", proc_name.c_str() );
	Beep( 350, 400 );
	return true;
}

void Process::detatch() {
	// close our handle to the game if one exists
	if ( m_proc )
		CloseHandle( m_proc );
}

ulong_t Process::get_proc_id( const std::string &proc_name ) {
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms682489(v=vs.85).aspx
	auto snapshot = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
	if ( snapshot == INVALID_HANDLE_VALUE )
		return 0;

	// processentry32 contains info on a process from a list of processes residing within system address space when a snapshot is taken
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof( entry );

	if ( !Process32First( snapshot, &entry ) ) {
		CloseHandle( snapshot );
		return 0;
	}

	// loop through processes in the snapshot
	while ( Process32Next( snapshot, &entry ) ) {
		// we've found the process we want
		if ( !proc_name.compare( entry.szExeFile ) )
			break;
	}

	// close the handle to the snapshot
	CloseHandle( snapshot );

	// return the processid
	return entry.th32ProcessID;
}

bool Process::get_modules() {
	// no proc id means we cant create a snapshot for modules
	if ( m_proc_id == 0 )
		return false;

	MODULEENTRY32 entry;
	entry.dwSize = sizeof( MODULEENTRY32 );

	// create a snapshot of this process' modules
	auto snapshot = CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, m_proc_id );

	// push back all modules within snapshot to our vector
	while ( Module32Next( snapshot, &entry ) ) {
		char path[ MAX_PATH ];
		GetModuleFileNameEx( m_proc, entry.hModule, path, MAX_PATH );

		auto module = std::make_shared<Module>( Module( (uintptr_t)entry.hModule, (uintptr_t)entry.modBaseSize, entry.szModule, path ) );

		m_modules.push_back( module );
	}

	// close the handle to our snapshot
	CloseHandle( snapshot );

	// no modules pushed back
	if ( m_modules.empty() )
		return false;

	return true;
}

Module *Process::get_module( const std::string &name ) {
	// no modules to itterate
	if ( m_modules.empty() )
		return nullptr;

	// itterate through our stored modules
	for ( auto &it : m_modules ) {
		if ( !name.compare( it->get_image_name() ) )
			return it.get();
	}

	return nullptr;
}